package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SampleController {

    @GetMapping
    public String getSamplePage(){
        return "index";
    }

    @GetMapping("/hello")
    public String getHello(){
        return "hello_world";
    }
}
